Marionette = require 'backbone.marionette'
App        = require './application.coffee'

class Router extends Marionette.AppRouter
  appRoutes:
    "": "root"
    "email": "email"
    "size": "size"
    "message": "message"
    "font": "font"
    "done": "done"

module.exports = Router
