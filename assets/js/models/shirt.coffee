Backbone = require 'backbone'
config   = require '../config.coffee'

class Shirt extends Backbone.Model
  url: config.api_url

module.exports = Shirt
