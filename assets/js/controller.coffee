Marionette   = require 'backbone.marionette'
App          = require './application.coffee'
Shirt        = require './models/shirt.coffee'
EmailForm    = require './views/email_form.coffee'
SizeForm     = require './views/size_form.coffee'
MessageForm  = require './views/message_form.coffee'
FontForm     = require './views/font_form.coffee'
DoneView     = require './views/done.coffee'

get_shirt = ->
  shirt = App.request('shirt')
  if not shirt
    shirt = new Shirt
    App.reqres.setHandler('shirt', -> shirt)
  return shirt

class Controller
  root: ->
    App.request('router').navigate('/email', trigger: true)

  email: ->
    App.main.show(new EmailForm(model: get_shirt()))

  size: ->
    App.main.show(new SizeForm(model: get_shirt()))

  message: ->
    App.main.show(new MessageForm(model: get_shirt()))

  font: ->
    App.main.show(new FontForm(model: get_shirt()))

  done: ->
    App.main.show(new DoneView)

module.exports = Controller
