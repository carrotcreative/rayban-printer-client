$            = require 'jquery'
Backbone     = require 'backbone'
Backbone.$   = $
Marionette   = require 'backbone.marionette'
Marionette.$ = $
fast_click   = require 'fastclick'
Router       = require './router.coffee'
Controller   = require './controller.coffee'
App          = require './application.coffee'


fast_click(document.body)

App.on 'start', ->
  router = new Router(controller: new Controller)
  App.reqres.setHandler('router', -> router)
  Backbone.history.start(pushState: true)

App.start()
