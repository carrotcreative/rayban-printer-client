Marionette = require 'backbone.marionette'
App        = require '../application.coffee'
$          = require 'jquery'
require('fancybox')($)

class EmailForm extends Marionette.ItemView
  template: templates.email_form

  ui:
    'email': "#email"
    'terms': '#terms'
    'submit': "#submit"
    'error': '#error'
    'links': 'a.fancybox'

  events:
    'click @ui.submit': 'set_email'
    'click @ui.back': 'go_back'
    'keydown @ui.email': 'blur_on_enter_key'

  set_email: (e) ->
    e.preventDefault()
    if not @ui.terms.is(':checked')
      console.log('debugging')
      @show_error('Please agree to the terms and conditions.')
      return false

    if @ui.email.val().length > 0
      @model.set(email: @ui.email.val())
      App.request('router').navigate('size', trigger: true)
    else
      @show_error('Please enter your email.')

  show_error: (msg) ->
    @ui.error.text(msg)
    @ui.error.show()

  onShow: ->
    @init_link_modals()

  init_link_modals: ->
    @ui.links.fancybox
      openEffect: 'elastic'
      closeEffect: 'elastic'
      iframe: { preload: false }

  blur_on_enter_key: (e) ->
    if e.keyCode == 13
      @ui.email.blur()
      false

module.exports = EmailForm
