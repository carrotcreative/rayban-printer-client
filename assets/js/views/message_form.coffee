Marionette = require 'backbone.marionette'
App        = require '../application.coffee'
_          = require 'lodash'

class MessageForm extends Marionette.ItemView
  template: templates.message_form

  ui:
    'message': "#message"
    'submit': "#submit"
    'back': '#back'
    'error': '#error'

  events:
    'click @ui.submit': 'set_message'
    'click @ui.back': 'go_back'
    'keydown @ui.message': 'blur_on_enter_key'

  set_message: (e) ->
    e.preventDefault()
    if not @validate_word_length() then return
    if @ui.message.val().length > 0
      @model.set(message: @ui.message.val().toUpperCase())
      App.request('router').navigate('font', trigger: true)
    else
      @show_error('Please enter a message.')

  onShow: ->
    @ui.message.focus()

  show_error: (msg) ->
    @ui.error.text(msg)
    @ui.error.show()

  go_back: ->
    App.request('router').navigate('size', trigger: true)

  blur_on_enter_key: (e) ->
    if e.keyCode == 13
      @ui.message.blur()
      return false

  validate_word_length: (e) ->
    invalid_word = _.find(@ui.message.val().split(' '), (str) -> str.length > 16)
    if invalid_word
      @show_error("Sorry, the limit for any single word is 16 characters. Please shorten the word #{invalid_word}")
      return false
    return true

module.exports = MessageForm
