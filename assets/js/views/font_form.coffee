Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class MessageForm extends Marionette.ItemView
  template: templates.font_form

  ui:
    'font': "input[name='font']:checked"
    'submit': 'input#submit'
    'preview': '#preview'
    'back': '#back'
    'error': '#error'
    'loading': '#loading'

  events:
    'click @ui.submit': 'set_font'
    'click @ui.font': 'update_font_preview'
    'click @ui.back': 'go_back'

  set_font: (e) ->
    e.preventDefault()
    @bindUIElements()
    @ui.loading.show()
    @model.set(font: @ui.font.val())
    @model.save null,
      success: (shirt, res) =>
        @ui.loading.hide()
        console.log('success')
        console.log(shirt, res)
        App.request('router').navigate('done', trigger: true)
      error: (a, b, c) =>
        @ui.loading.hide()
        res = JSON.parse(b.responseText)
        if not res.success
          @ui.error.text(res.human_readable)
          @ui.error.show()
        console.log('error', a, b, c)

  update_font_preview: ->
    @bindUIElements()
    @ui.preview.removeClass().addClass(@ui.font.val())

  go_back: ->
    App.request('router').navigate('/message', trigger: true)

module.exports = MessageForm
