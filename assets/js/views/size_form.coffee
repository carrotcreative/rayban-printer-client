Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class SizeForm extends Marionette.ItemView
  template: templates.size_form

  ui:
    'size': "input[name='size']:checked"
    'submit': 'input#submit'
    'back': '#back'

  events:
    'click @ui.submit': 'set_size'
    'click @ui.back': 'go_back'

  set_size: (e) ->
    e.preventDefault()
    @bindUIElements()
    @model.set(size: @ui.size.val())
    App.request('router').navigate('message', trigger: true)

  go_back: ->
    App.request('router').navigate('email', trigger: true)

module.exports = SizeForm
