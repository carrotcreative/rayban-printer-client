Marionette = require 'backbone.marionette'
App        = require '../application.coffee'

class DoneView extends Marionette.ItemView
  template: templates.done_view

  ui:
    'reset': '#reset'

  events:
    'click @ui.reset': 'reset'

  reset: ->
    App.request('router').navigate('/', trigger: true)

module.exports = DoneView
