axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
css_pipeline = require 'css-pipeline'
browserify   = require 'roots-browserify'
templates    = require 'client-templates'

module.exports =
  ignores: ['readme.md', '**/layout.*', '**/_*', '.gitignore', 'ship.*conf']

  extensions: [
    browserify(files: 'assets/js/main.coffee', out: 'js/build.js')
    css_pipeline(files: 'assets/css/*.styl')
    templates(base: 'assets/js/templates')
  ]

  stylus:
    use: [axis(), rupture(), autoprefixer()]
    sourcemap: true

  'coffee-script':
    sourcemap: true

  jade:
    pretty: true

  server:
    routes:
      "./!(*.*)"  : "/index.html"
      "**/!(*.*)" : "/index.html"
      "./*.html"  : "/index.html"
      "**/*.html" : "/index.html"
