# rayban-printer-client

The front end for printing out Rayban T-shirts at SXSW like a boss.

### Setup

- make sure [node.js](http://nodejs.org) and [roots](http://roots.cx) are installed
- clone this repo down and `cd` into the folder
- run `npm install`
- run `roots watch`
- ???
- get money

### Deploying

- If you just want to compile the production build, run `roots compile -e production` and it will build to public.
- To deploy your site with a single command, run `roots deploy -to XXX` with `XXX` being whichever [ship](https://github.com/carrot/ship#usage) deployer you want to use.

### What if we run out of Shirts?

In the unlikely event we run out of a shirt size, simply add `disabled='disabled'` to the size inputs in `size_form.jade`. The styles are already written.
